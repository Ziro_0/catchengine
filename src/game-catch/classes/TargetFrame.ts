export default class TargetFrame extends Phaser.Image {
    game: any;

    constructor(game) {
        super(game, game.world.centerX, game.world.centerY, 'target_frame');
        this.anchor.setTo(0.5);
        this.game.groups.targets.add(this);
    }
}
