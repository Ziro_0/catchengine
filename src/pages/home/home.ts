import { Component } from '@angular/core';
import { Platform, MenuController, NavController, NavParams } from 'ionic-angular';
import { Game as GameCatch } from '../../game-catch/game';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    showAlertMessage: boolean = true;
    autoplayTimer: any = null;
    isDemo: boolean = true;
    isIos: boolean = false;
    isTutorial: boolean = false;
    finalStageNumber: number = 7;
    tutorialStage: number = 1;
    visibleState: string = 'visible';
    randomLeft: number = 0;
    opponentPoints: number = 0;
    opponentPointsLoading: boolean = true;
    bottomPosition: string = '-100px';
    staticTimerFn: any = null;
    internetAvailable: boolean = true;
    isBumper: boolean = false;
    bumperType: any = 1;
    problemText: string = null;
    loadingController: any = null;
    singleNav: any = null;
    resultsArray: any = {
        rightbubbles: 0,
        wrongbubbles: 0,
        rightTreats: 0,
        wrongTreats: 0,
        totalbubbles: 0
    };
    livesRemaining: boolean = false;
    resultDeclared: boolean = false;
    persistantLives: number = 10;
    perishableLives: number = 5;

    //Change bubble bottom position
    changeBottomIndex: number = 0;

    bubbles: any = [];
    bubblesSorted: any = [];
    declaredResultData: any = {};
    popIndex: number = 0;

    gamingIndex: number = 0;

    //Timer seconds
    timerNum: number = 60;
    points: any = 0;
    bumperPoints: number = 0;
    onEnded: boolean = false;
    onLeft: boolean = false;
    problemsOver: boolean = false;
    chosenColor: string = 'red';
    bubblePopRightIndex = 0;
    bubblePopWrongIndex = 0;
    level = 0;
    boxWidth = 0;
    boxHeight = 0;
    bubbleWidthParameter = 0;
    tableMoney = 5;
    levelIncreasing: boolean = true;
    freshlyLoaded: boolean = true;
    showLoading: boolean = false;

    staticTimer: number = 2;
    staticTimerBubbleFn: any;

    //For demo
    config: any = {};
    levelConfig: any = {
        hitLosePoints: 0,
        jumpPoints: 1,
        phrases: ['Awesome!', 'Jhakaas!', 'Amazing!', 'WoW!'],
        characterChanges: 3,
        characterChangesBack: 2,
        showLivesLost: true,
        jumpSpeed: -950,
        gravityUp: 2800,
        gravityDown: 5000,
    };
    catchEngineConfig: any = {
        showLivesLost: true,
        extraPoints: 2,
        jumpsRequiredForExtra: 3,
        hitLosePoints: 0,
        jumpPoints: 1,
        expToLevelup: 3,
        gravity: 1,
        jumpSpeed: 10,
        startTimeSecs: 30,
        
        maxTargetDuration: 2000,
        minTargetDuration: 650,

        backgroundImageFile: 'assets/game-catch/images/background.png',

        targetFrameFile: 'assets/game-catch/images/red_star_target_frame.png',
        targetImageFile: 'assets/game-catch/images/blue_star_target_image.png',

        pointsPerMiss: 1,
        pointsPerHit: 5,
        targetHitRatioThreshold: 0.9,//0.96,

        numConsecHitsForExtraLife: 2,

        targetHoldDelay: 300,
    };

    gameInstance: any;
    gameStyle: any = {
        width: '100%',
        height: '100%'
    };
    paramsGameId: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public menu: MenuController) {
        this.paramsGameId = navParams.get('paramsGameId');
    }

    ionViewDidLoad() {
        this.calculateBoxWidth();
        this.startNow(this.catchEngineConfig.startTimeSecs);
    }

    onReady() {
        console.log('the game has finished loading and is ready to play');
    }

    perfectCallback(gameInstance) {
        console.log('perfectCallback called in home.ts!');
        gameInstance.showPerfect();
    }

    /*
    normalLandingCallback() {
        console.log('Normal landing callback listened in home.ts!');
        this.right();
    }
    */

    livesGainedCallback(gameInstance) {
        console.log('Lives gained callback listened in home.ts!');
        this.oneUp(gameInstance);
    }

    livesLostCallback(gameInstance) {
        console.log('Lives lost callback listened in home.ts!');
        this.wrong(gameInstance);
    }

    /*
    jumpCallback() {
        console.log('Player just jumped');
    }
    */

    /*
    playerShrinkCallback() {
        console.log('Shrinking the player');
    }
    */

    addPointCallback(points) {
       console.log('Adding a point', points);
       this.points = this.points + points;
    }

    targetSpeedCallback(speed) {
        let gameState = this.gameInstance ? this.gameInstance.state.getCurrentState() : null;
        if (!gameState || gameState.key !== 'GameState') {
            return;
        }

        console.log(`targetSpeedCallback. set target speed to ${speed}`);
        gameState.targetSpeed = speed;
        gameState.isTargetSpeedFixed = true;
    }

    targetSpeedRandomCallback() {
        let gameState = this.gameInstance ? this.gameInstance.state.getCurrentState() : null;
        if (!gameState || gameState.key !== 'GameState') {
            return;
        }

        console.log('targetSpeedRandomCallback. target speed will be random');
        gameState.isTargetSpeedFixed = false;
    }

    catchEngineListener() {
        this.gameInstance = new GameCatch(this.boxWidth, this.boxHeight);
        this.gameInstance.startGame(this.catchEngineConfig);

        this.gameInstance.listen('lives-lost', this.livesLostCallback.bind(this));
        this.gameInstance.listen('lives-gained', this.livesGainedCallback.bind(this));
        this.gameInstance.listen('add-point', this.addPointCallback.bind(this));
        this.gameInstance.listen('perfect', this.perfectCallback.bind(this));
        this.gameInstance.listen('target-speed', this.targetSpeedCallback.bind(this));
        this.gameInstance.listen('target-speed-random', this.targetSpeedRandomCallback.bind(this));
        //this.gameInstance.listen('jump', this.jumpCallback.bind(this));
        //this.gameInstance.listen('normal-landing', this.normalLandingCallback.bind(this));
        //this.gameInstance.listen('start-shrinking', this.playerShrinkCallback.bind(this));

        this.gameInstance.listen('ready', this.onReady.bind(this));
    }

    startNow(num) {
        this.gameStyle = {
            width: this.boxWidth + 'px',
            height: this.boxHeight + 'px'
        };

        switch (this.paramsGameId) {
            case 19:
                this.catchEngineListener();
                break;
        }

        //Information to be used!
        this.showAlertMessage = false;
        this.onEnded = false;
        this.onLeft = false;

        this.timerNum = num; //Value of the timer
        this.perishableLives = 5; //Initial per contest lives
        this.persistantLives = 0; //Final bought lives

        this.points = 0; //Points scored

        this.checkLivesAvailable(); //This function checks if lives are available or not! It ends your contest if your lives are over.
    }

    right() {
        this.points = this.points + this.levelConfig.jumpPoints;
    }
    
    oneUp(gameInstance) {
        this.perishableLives = this.perishableLives + 1;
        gameInstance.showLivesLost(1);
    }

    wrong(gameInstance) {
        this.points = this.points - this.levelConfig.hitLosePoints;

        if (this.perishableLives > 0) {
            gameInstance.showLivesLost(-1);
            this.perishableLives = this.perishableLives - 1;
        } else {
            if (this.tableMoney != 0) {
                gameInstance.showLivesLost(-1);
                this.persistantLives = this.persistantLives - 1;
            }
        }
        this.checkLivesAvailable();
    }

    checkLivesAvailable() {
        if (this.perishableLives <= 0 && this.persistantLives <= 0) {
            this.livesRemaining = false;
            this.onEnd();
        } else {
            this.livesRemaining = true;
        }
    }

    calculateBoxWidth() {
        this.boxWidth = this.platform.width();
        this.boxHeight = this.platform.height();

        // if (this.isIos) {
        //     this.boxHeight = this.boxHeight - 50;
        // }
    }

    problemsOverFn(problemText?) {
        this.problemText = 'Some problem occurred, Please try again!';
        if (problemText) {
            this.problemText = problemText;
        }
        this.timerNum = 0;
        this.problemsOver = true;
        this.clearTimer();
    }

    nextStage() {
        this.tutorialStage = this.tutorialStage + 1;

        if (this.tutorialStage == 4) {
            this.perishableLives = 0;
        }
        if (this.tutorialStage == 5) {
            this.persistantLives = 0;
        }

        if (this.tutorialStage == this.finalStageNumber + 1) {
            this.dismiss();
        }
    }

    chooseLevel() {
        if (this.level < this.config.levels + 1 && this.levelIncreasing) {
            this.level++;
        }

        if (this.level > 0 && !this.levelIncreasing) {
            this.level--;
        }

        if (this.level == 0) {
            this.level = 2;
            this.levelIncreasing = true;
        }

        if (this.level == this.config.levels + 1) {
            this.level = this.config.levels - 1;
            this.levelIncreasing = false;
        }

        this.levelConfig = this.config[this.level];
    }

    generateRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    dismiss() {
        this.navCtrl.pop();
    }

    confirmDismissal() {
        this.showAlertMessage = false;
        this.onEnded = true;
        this.onLeft = true;
        this.clearTimer();

        setTimeout(() => {
            this.clearTimer();
        }, 1000);

        this.menu.enable(true);
    }

    clearTimer() {
        if (this.staticTimerFn) {
            clearTimeout(this.staticTimerFn);
            this.staticTimerFn = null;
        }
    }

    ionViewDidEnter() {
        this.menu.enable(false);
    }

    ionViewCanLeave() {
        if (!this.onEnded && !this.problemsOver && !this.isTutorial) {
        } else {
            this.menu.enable(true);
        }
    }

    getBubbleParamsAccToWidth() {
        let width = this.boxWidth; //width of box
        let singleBoxParams = {};
        this.levelConfig.box.forEach(singleBox => {
            if (singleBox.minWidth) {
                if (singleBox.maxWidth) {
                    //Check width between minWidth and maxWidth
                    if (width >= singleBox.minWidth && width <= singleBox.maxWidth) {
                        singleBoxParams = singleBox;
                    }
                } else {
                    //Check width >= minWidth
                    if (width >= singleBox.minWidth) {
                        singleBoxParams = singleBox;
                    }
                }
            } else {
                //Check width between 0 and maxWidth
                if (width >= 0 && width <= singleBox.maxWidth) {
                    singleBoxParams = singleBox;
                }
            }
        });

        return singleBoxParams;
    }

    _arrayRandom(len, min, max, unique) {
        var len = len ? len : 10,
            min = min !== undefined ? min : 1,
            max = max !== undefined ? max : 100,
            unique = unique ? unique : false,
            toReturn = [],
            tempObj = {},
            i = 0;

        if (unique === true) {
            for (; i < len; i++) {
                var randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
                if (tempObj['key_' + randomInt] === undefined) {
                    tempObj['key_' + randomInt] = randomInt;
                    toReturn.push(randomInt);
                } else {
                    i--;
                }
            }
        } else {
            for (; i < len; i++) {
                toReturn.push(Math.floor(Math.random() * (max - min + min)));
            }
        }

        return toReturn;
    }

    removeLoading() {
        if (this.showLoading) {
            this.showLoading = false;
            this.loadingController.dismiss();
        }
    }

    onTimerFinished() {
        this.onEnd();
    }

    onEnd() {
        if (!this.onEnded) {
            this.gameInstance.endGame();
        }
        this.clearTimer();
        this.onEnded = true;
        this.onLeft = true;

        if (this.isBumper || this.isDemo) {
            if (this.livesRemaining) {
                this.dismiss();
            }
        } else {
            if (this.livesRemaining) {
            }
        }
    }
}
