export default class Perfect extends Phaser.Text {
    game: any;
    anchor: any;
    font: any;
    fontSize: any;
    fill: any;
    strokeThickness: any;
    stroke: any;
    x: any;
    y: any;

    constructor(game) {
        let x = game.world.centerX;
        let y = game.world.centerY;

        super(game, x, y, "PERFECT!");
        this.game.groups.gui.add(this);
        this.anchor.setTo(0.5);

        this.font = 'roboto';
        this.fontSize = 80;
        this.fill = '#fff';
        this.strokeThickness = 8;
        this.stroke = '#cd10da';

        this.game.add
            .tween(this)
            .to({ x: '300', y: '86', alpha: 0 }, 500, 'Linear', true)
            .onComplete.add(() => {
                this.destroy();
            });
    }
}
