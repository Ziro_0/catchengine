import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ITimer} from '../../interfaces/itimer';


@Component({
    selector: 'timer',
    templateUrl: 'timer.html'
})
export class TimerComponent {

    @Input() timeInSeconds: number;
    @Input() showHours: string;
    @Input() dontshowMinutes: string;
    @Output() end = new EventEmitter();
    @Output() countdown = new EventEmitter();
    @Output() update = new EventEmitter();

    public timer: ITimer;
    public timeout: any;
    public timerEnds: any;
    public countCalled: boolean = false;

    constructor() {
    }

    onEnd() {
      this.end.emit();
    }

    adjustTimings() {
      this.timerEnds = new Date(new Date().getTime() + this.timeInSeconds * 1000);
    }

    ngOnInit() {
        this.initTimer();
    }

    ngOnChanges() {
        this.initTimer();
    }

    hasFinished() {
        return this.timer.hasFinished;
    }

    initTimer() {
        this.countCalled = false;
        this.adjustTimings();
        if( this.timeout ) {
            clearTimeout( this.timeout );
            this.timeout = null;
        }

        if(!this.timeInSeconds) { this.timeInSeconds = 0; }

        this.timer = <ITimer>{
            seconds: this.timeInSeconds,
            runTimer: false,
            hasStarted: false,
            hasFinished: false,
            secondsRemaining: this.timeInSeconds
        };

        this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);

        if( this.timeInSeconds != 0 ) {
          this.stopTimer();
          this.startTimer();
        }
    }

    startTimer() {
        this.timer.hasStarted = true;
        this.timer.runTimer = true;
        this.timerTick();
    }

    pauseTimer() {
        this.timer.runTimer = false;
    }

    stopTimer() {
      if( this.timeout ) {
          clearTimeout( this.timeout );
          this.timeout = null;
      }

      this.timer.runTimer = false;
    }

    resumeTimer() {
        this.startTimer();
    }

    callCount() {
      if( !this.countCalled ) {
        this.countCalled = true;
        if( this.countdown ) {
          this.countdown.emit();
        }
      }
    }

    timerTick() {
        this.timeout = setTimeout(() => {
            if (!this.timer.runTimer) { return; }
            this.timer.secondsRemaining = Math.ceil((this.timerEnds.getTime() - new Date().getTime())/1000);
            this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
            if (this.timer.secondsRemaining > 0) {
                this.timerTick();
                this.update.emit({
                  time: this.timer.secondsRemaining
                });
                if( this.timer.secondsRemaining ==5 ) {
                  this.callCount();
                }
            } else {
                this.timer.hasFinished = true;
                this.onEnd();
                if( this.update ) {
                  this.update.emit({
                    time: 0
                  });
                }
            }
        }, 1000);
    }

    getSecondsAsDigitalClock(inputSeconds: number) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        if( this.showHours == "true" ) {
          return hoursString + ':' + minutesString + ':' + secondsString;
        } else {
          return minutesString + ':' + secondsString;
        }
    }

    ngOnDestroy() {
      this.stopTimer();
    }

}
