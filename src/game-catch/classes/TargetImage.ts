export default class TargetImage extends Phaser.Image {
    game: any;
    
    onDown: Phaser.Signal;
    onRoundStart: Phaser.Signal;

    _moveTween: Phaser.Tween;
    _xStart: number;
    _xMiddle: number;
    _xEnd: number;
    _isPointerDown: boolean;
    _durationPendingValue: number = NaN;

    constructor(game: any, duration: number) {
        super(game, game.world.centerX, game.world.centerY, 'target_image');
        this.anchor.setTo(0.5);
        this.game.groups.targets.add(this);
        
        //setup tween variables
        const widthOffset = this.width / 2;
        this._xStart = -widthOffset;
        this._xMiddle = -this.game.world.centerX;
        this._xEnd = this.game.world.width + widthOffset;
        
        //set starting position
        this.x = this._xStart;

        //setup the tweens
        this._initTweens(duration);

        //setup signals
        this.onDown = new Phaser.Signal();
        this.onRoundStart = new Phaser.Signal();
    }

    set duration(value) {
        this._durationPendingValue = value;
    }

    get isPaused() {
        return(this._moveTween.isPaused);
    }

    set isPaused(value) {
        this._moveTween.isPaused = value;
    }

    start() {
      this._moveTween.start();
    }

    update() {
        if (!this.game.targetFrame) {
            return;
        }

        if (this.game.input.activePointer.isDown) {
            this._handleInputDown();
        } else {
            this._handleInputUp();
        }
    }

    _handleInputDown() {
        if (this._isPointerDown) {
            return;
        }

        this._isPointerDown = true;
        this.onDown.dispatch();
    }

    _handleInputUp() {
        this._isPointerDown = false;
    }

    _initTweens(duration: number) {
        const tweenDuration = duration / 2;
        this._moveTween = this.game.add.tween(this)
            .to( { x: this._xMiddle }, tweenDuration)
            .to( { x: this._xEnd}, tweenDuration)
            .loop(true);

        this._moveTween.onStart.add(this._onTweenBegin, this);
        this._moveTween.onLoop.add(this._onTweenBegin, this);
    }

    _onTweenBegin() {
        if (!isNaN(this._durationPendingValue)) {
            this._moveTween.updateTweenData('duration',
                this._durationPendingValue / 2, -1);
            this._durationPendingValue = NaN;
        }

        this.onRoundStart.dispatch();
    }
}
