// import pixi, p2 and phaser ce
import 'pixi';
import 'p2';
import * as Phaser from 'phaser-ce';

// import states
import BootState from './states/BootState';
import PreloadState from './states/PreloadState';
import MainMenuState from './states/MainMenuState';
import GameState from './states/GameState';

import Heart from './classes/Heart';
import Perfect from './classes/Perfect';

/**
 * Main entry game class
 * @export
 * @class Game
 * @extends {Phaser.Game}
 */
export class Game extends Phaser.Game {
    config: any;
    listenerMapping: any = {};

    /**
     * Creates an instance of Game.
     * @memberof Game
     */
    constructor(width, height) {
        // call parent constructor
        // super(width, height, Phaser.CANVAS, 'game', null);
        super(640, 1136, Phaser.CANVAS, 'game', null);

        // add some game states
        this.state.add('BootState', new BootState(this));
        this.state.add('PreloadState', new PreloadState(this));
        this.state.add('MainMenuState', new MainMenuState(this));
        this.state.add('GameState', new GameState(this));
    }

    startGame(config = false) {
        console.log('game has started');
        this.config = config;
        console.log(this.config);
        this.state.start('BootState');
    }

    listen(listenValue, cb) {
        this.listenerMapping[listenValue] = cb;
    }

    resurrect() {
        // this.core.respawn();
    }

    showLivesLost(num) {
        if (!this.config.showLivesLost) return;
        new Heart(this, num);
    }

    showPerfect() {
        new Perfect(this);
    }

    endGame() {
        this.paused = true;
        setTimeout(() => {
            this.destroy();
        }, 100);
    }
}
