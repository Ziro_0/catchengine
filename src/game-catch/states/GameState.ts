import TargetFrame from '../classes/TargetFrame';
import TargetImage from '../classes/TargetImage';
import { irandomRange } from '../util/util';

export default class GameState {
    game: any;
    isTargetSpeedFixed: boolean = false;

    _timer: Phaser.Timer;
    _numConsecHits: number = 0;
    _missed: boolean = false;
    _alreadyTriedThisRound: boolean = false;

    constructor(game) {
        this.game = game;
    }

    create() {
        console.log('Game state [create] started');
        
        this.isTargetSpeedFixed = false;

        this._callListenerMap('ready', this.game);

        this._createBackgroundImage();

        // create groups
        this.game.groups = { };
        ['targets', 'gui'].forEach(
            group => (this.game.groups[group] = this.game.add.group())
        );

        // create target frame and image
        this.game.targetFrame = new TargetFrame(this.game);
        
        this.game.targetImage = new TargetImage(this.game, 1500);
        this.game.targetImage.onDown.add(this._onTargetImageDown, this);
        this.game.targetImage.onRoundStart.add(this._onTargetImageRoundStart, this);
        this.targetSpeed = this._calcRandomTargetDuration();

        // setup process timer
        const AUTO_DESTROY = false;
        this._timer = this.game.time.create(AUTO_DESTROY);
        this._timer.start();

        this.game.targetImage.start();
    }

    set targetSpeed(value) {
        console.log(`setting target speed to ${value}`);
        this.game.targetImage.duration = value;
    }

    /**
     * @private
     */
    _addConsecHit() {
        this._numConsecHits = this._numConsecHits + 1;
        if (this._numConsecHits >= this.game.config.numConsecHitsForExtraLife) {
            this._numConsecHits = 0;
            this._callListenerMap('lives-gained', this.game);
        }
    }

    /**
     * @private
     */
    _calcDistanceRatio() {
        const distance = Math.abs(this.game.targetFrame.x - this.game.targetImage.x);
        const ratio = 1.0 - distance / this.game.targetFrame.x;
        return(ratio);
    }

    /**
     * @private
     */
    _calcRandomTargetDuration() {
        const config = this.game.config;
        return(irandomRange(
            config.minTargetDuration,
            config.maxTargetDuration));
    }

    /**
     * @private
     */
    _callListenerMap(key: string, ...args) {
        const callback:Function = this.game.listenerMapping[key];
        if (callback) {
            callback.apply(null, args);
        }
    }

    /**
     * @private
     */
    _checkIfAlreadyTriesThisRound() {
        if (this._alreadyTriedThisRound) {
            return(true);
        }

        this._alreadyTriedThisRound = true;
        return(false);
    }

    /**
     * @private
     */
    _createBackgroundImage() {
        let image = this.game.add.image(
            this.game.world.centerX,
            this.game.world.centerY,
            'background');
        image.anchor.set(0.5);

        image.width = this.game.world.width;
        image.scale.y = image.scale.x;
        if (image.height < this.game.world.height) {
            image.height = this.game.world.height;
            image.scale.x = image.scale.y;
        }
    }

    /**
     * @private
     */
    _didPlayerHitTarget() {
        const ratio = this._calcDistanceRatio();
        console.log(`ratio: ${ratio}`);
        return(ratio >= this.game.config.targetHitRatioThreshold);
    }

    /**
     * @private
     */
    _isTargetWithinTriableRange() {
        const x = this.game.targetImage.x;
        return(0 < x && x < this.game.world.width);
    }

    /**
     * @private
     */
    _onTargetImageDown() {
        if (!this._isTargetWithinTriableRange()) {
            return;
        }

        if (this._checkIfAlreadyTriesThisRound()) {
            return;
        }

        this._processShot();
    }

    /**
     * @private
     */
    _onTargetImageRoundStart() {
        if (this._missed) {
            this._missed = false;
            this._callListenerMap('lives-lost', this.game);
        }
        
        this._alreadyTriedThisRound = false;
    }

    /**
     * @private
     */
    _onTimerComplete() {
        this.game.targetImage.isPaused = false;
    }

    /**
     * @private
     */
    _presentMiss() {
        this._missed = true;
        this._numConsecHits = 0;

        const points = this.game.config.pointsPerMiss;
        this._callListenerMap('add-point', points);
    }

    /**
     * @private
     */
    _presentPerfect() {
        this.game.targetImage.x = this.game.world.centerX;
        
        this._addConsecHit();
        this._callListenerMap('add-point', this.game.config.pointsPerHit);
        this._callListenerMap('perfect', this.game);
    }

    /**
     * @private
     */
    _processShot() {
        if (this._didPlayerHitTarget()) {
            this._presentPerfect();
        } else {
            this._presentMiss();
        }

        this._stunTarget();

        if (!this.isTargetSpeedFixed) {
            this.targetSpeed = this._calcRandomTargetDuration();
        }
    }

    /**
     * @private
     */
    _stunTarget() {
        this.game.targetImage.isPaused = true;
        this._timer.add(this.game.config.targetHoldDelay, this._onTimerComplete, this);
    }
}
