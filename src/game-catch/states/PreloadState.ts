export default class PreloadState {
    game: any;
    preloadBar: any;

    constructor(game) {
        this.game = game;
    }

    preload() {
        this.preloadBar = this.game.add.image(
          this.game.world.centerX - 200,
          this.game.world.centerY - 200,
          'preload_bar');

        this.game.load.setPreloadSprite(this.preloadBar);

        const config = this.game.config;

        this.game.load.image('background', config.backgroundImageFile);
        this.game.load.image('heart', 'assets/game-jump/images/heart.png');
        this.game.load.image('target_frame', config.targetFrameFile);
        this.game.load.image('target_image', config.targetImageFile);
    }

    create() {
        this.game.state.start('MainMenuState');
    }
}
